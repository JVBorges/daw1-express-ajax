const fs = require('fs');
const express = require('express');
const app = express();


app.use(express.urlencoded({ extended: false }));
app.use(express.static(`${__dirname}/static`));

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/static/index.html`);
});

app.get('/pessoas', (req, res) => {
  const { query, XPTO } = req.query;

  fs.readFile(`${__dirname}/static/data.json`, 'utf-8', (err, data) => {
    if (err)
      return res.status(500).json('Erro ao ler arquivo');

    let pessoas = JSON.parse(data);
    pessoas = pessoas.filter(pessoa => pessoa[XPTO].toString().toUpperCase().replace(/[^0-9a-zA-Z]+/g, '').includes(query.toUpperCase()));

    res.json(pessoas);
  });
});

app.listen(3000, () => console.log('Serve running at port 3000'));